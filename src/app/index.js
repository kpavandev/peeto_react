var React = require('react')
var ReactDOM = require('react-dom')

// CSS Module
require('./css/index.css')

//Modules Requried
var Cardview = require('./card');

class ApplicationExp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            url: "https://jsonblob.com/api/jsonBlob/b235e32f-8250-11e7-8e2e-893ffec7f2e1",
            loc: "",
            locValue: "Enter location or Click locate me"
        }
    };

    componentWillMount() {
    }

    //Fetching JSON Data 
    fetchData(e) {
        var address = "";
        address = this.refs.newitem.value;
        return fetch(this.state.url)
            .then((response) => response.json())
            .then((response) => {
                this.setState({
                    loc: response.ship.ship_name // Here we get the location if the service is available
                })
                var loc = this.state.loc;
                var pos = address.search(new RegExp(loc, "i")); //comparing the address provided by user and the ship_name 
                if (pos >= 0) {
                    this.setState({
                        data: response.menu.menu_items, //if address is matched, Storing the the Json data to data array
                    })
                }
                else {
                    this.setState({
                        data: [],
                        locValue: "Coming Soon!!!" // if address is not matched, this value will be printed on webpage
                    })
                }
            })
    }

    componentDidMount() {
        var __this = this;
        var options = {
            componentRestrictions: { country: "IN" }
        };

        // This function will autocomplete the address typed in search bar
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('searchInput'), options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var msg = "Address: " + address;
                msg += "\nLatitude: " + latitude;
                msg += "\nLongitude: " + longitude;
                __this.setState({
                    locValue: msg
                })
                __this.fetchData();
            });
        });
    }

    // Get the location when locate me button clicked
    locate() {
        var lat, lan;
        var __this = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                lat = position.coords.latitude;
                lan = position.coords.longitude;
                var latlng = new google.maps.LatLng(lat, lan);
                var geocoder = geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            __this.refs.newitem.value = results[0].formatted_address;
                            var address = results[0].formatted_address;
                            var latitude = lat;
                            var longitude = lan;
                            var msg = "Address: " + address;
                            msg += "\nLatitude: " + latitude;
                            msg += "\nLongitude: " + longitude;
                            __this.setState({
                                locValue: msg
                            })
                            __this.fetchData();
                        }
                    }
                    else {
                        console.log("err")
                    }
                });
            });
        }
    }

    render() {
        var card = this.state.data;
        card = card.map(function (fdata, key) {
            return (
                 <Cardview fdata={fdata} key={key} />  /*  card template module is created, all the data
                                                         fetch from api call will be sent to card module 
                                                        to display in webpage  */
            )
        }.bind(this));

        return (
            <div>
                <div className="contents">
                    <center>
                        <input id="searchInput" name="searchplace" required ref="newitem" type="text" placeholder="Enter Your Location"></input>
                        <button className="locateMe" onClick={this.locate.bind(this)}>Locate Me</button>
                        <p className="location">{this.state.locValue} </p>
                    </center>
                </div><br />
                <span>{card}</span>
            </div>
        );
    }
}

ReactDOM.render(<ApplicationExp />, document.getElementById('app'));