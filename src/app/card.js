var React = require('react')

// CSS Module
require('./css/card.css')

class Cardview extends React.Component{
    constructor(props) {
        super(props)
    };
    
    render(){
        return(
            // data to display in webpage be provided by ApplicationExp module
            <div className="card-list">
             <span className="item-name">{this.props.fdata.menu_item_name}</span><br/>
             <span className="item-category">Category: {this.props.fdata.category_name}</span><br/>
             <span className="item-price">₹: {this.props.fdata.price}</span><br/>
             <span className="item-type">{this.itemType(this.props.fdata.is_veg.toString())}</span>
            </div>
        )
    }

    itemType(val){
        var item_type = document.getElementsByClassName("item-type");
        if(val=="true"){
             return item_type.innerHTML = "VEG"
        }
        else{
           return item_type.innerHTML = "NON-VEG"
        }
    }
}

module.exports = Cardview;

